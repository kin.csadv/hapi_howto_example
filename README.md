# hapi_howto_example

This is hapi.js howto example. It includes:

- auth samples
- request/response schemes
- ORM sequelize
- error handeling
- swagger/openapi
- autotest generation


## Installation

```
npm i && npm run dbinit
```


## Startup

```
node server.js
```


## Swagger/OpenAPI

open the http://localhost:3030/documentation

or

```
curl http://localhost:3030/documentation.json
```


## Test

```
npm test
```


## Linting

```
npm run lint
```

or

```
npm run lint-fix
```
